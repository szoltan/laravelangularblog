<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'articles';

    /**
     * Append custom attributes to the model
     * @var array
     */
    protected $appends = array();

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'body', 'user_id', 'intro'];

    public function user()
    {
        return $this->belongsTo('App\User');
        // return $this->belongsTo('App\User')->select(['id', 'name', 'email']);
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }
}