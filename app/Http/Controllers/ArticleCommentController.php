<?php
/**
 * @package larablog.
 * @author Zoltan Szanto <mrbig00@gmail.com>
 * @since 2016/06/12
 */

namespace App\Http\Controllers;
use App\Comment;
use Illuminate\Http\Request;
use App\Http\Requests;
use Response;
use Input;
use Validator;

class ArticleCommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param $id
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($id, Request $request)
    {
        $rules = array(
            'content' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Response::json(
                [
                    'errors' =>
                        array_filter([
                            'comment' => $validator->errors()->first('content'),
                        ])
                ], 400);
        } else {
            $fields = $request->all();
            $fields['user_id'] = \Auth::user()->id;
            $fields['article_id'] = $id;
            $article = Comment::create($fields);
            return Response::json([
                'message' => 'Comment added successfully',
                'data' => $this->transform($article)
            ]);
        }
    }

    private function transform($comment)
    {
        return [
            'comment_id' => $comment['id'],
            'author' => $comment['user']['name'],
            'content' => $comment['comments'],
            'date' => $comment['created_at']->toDateTimeString()
        ];
    }
}