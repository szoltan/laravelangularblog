<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Article;
use Response;
use Input;
use Validator;

class ArticlesController extends Controller
{
    public function __construct(Request $request)
    {

        $this->middleware('jwt.auth', ['except' => ['authenticate', 'index', 'show']]);
        $this->middleware('adminOnly', ['only' => 'update', 'store', 'destroy']);
    }

    public function index(Request $request)
    {
        $search_term = $request->input('search');
        $limit = $request->input('limit', 5);

        if ($search_term && $search_term != '') {
            $articles = Article::orderBy('id', 'DESC')->where('title', 'LIKE', "%$search_term%")
                ->orWhere('body', 'LIKE', "%$search_term%")
                ->orWhere('intro', 'LIKE', "%$search_term%")
                ->with(
                    array('User' => function ($query) {
                        $query->select('id', 'name');
                    })
                )->select('id', 'title', 'body', 'intro', 'user_id', 'created_at')->paginate($limit);

            $articles->appends(array(
                'search' => $search_term,
                'limit' => $limit
            ));
        } else {
            $articles = Article::orderBy('id', 'DESC')->with(
                array('User' => function ($query) {
                    $query->select('id', 'name');
                })
            )->select('id', 'title', 'body', 'intro', 'user_id', 'created_at')->paginate($limit);

            $articles->appends(array(
                'limit' => $limit
            ));
        }
        return Response::json($this->transformCollection($articles), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'title' => 'required',
            'body' => 'required',
            'intro' => 'required'
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Response::json(
                [
                    'errors' =>
                        array_filter([
                            'title' => $validator->errors()->first('title'),
                            'body' => $validator->errors()->first('body'),
                            'intro' => $validator->errors()->first('intro'),
                        ])
                ], 400);
        } else {
            $fields = $request->all();
            $fields['user_id'] = \Auth::user()->id;
            $article = Article::create($fields);
            return Response::json([
                'message' => 'Article Created Succesfully',
                'data' => $this->transform($article)
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::with('user', 'comments.user')->select('id', 'title', 'body', 'intro', 'user_id', 'created_at')->find($id);

        if (!$article) {
            return Response::json([
                'error' => [
                    'message' => 'Article does not exist'
                ]
            ], 404);
        }

        $previous = Article::where('id', '<', $article->id)->max('id');
        $next = Article::where('id', '>', $article->id)->min('id');


        return Response::json([
            'previous_article_id' => $previous,
            'next_article_id' => $next,
            'data' => $this->transform($article)
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'title' => 'required',
            'body' => 'required',
            'intro' => 'required'
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Response::json(
                [
                    'errors' =>
                        array_filter([
                            'title' => $validator->errors()->first('title'),
                            'body' => $validator->errors()->first('body'),
                            'intro' => $validator->errors()->first('intro'),
                        ])
                ], 400);
        } else {

            $article = Article::find($id);
            $article->body = $request->body;
            $article->title = $request->title;
            $article->intro = $request->intro;
            $article->save();

            return Response::json([
                'message' => 'Article Updated Succesfully'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Article::destroy($id);
    }

    private function transformCollection($articles)
    {
        $articlesArray = $articles->toArray();
        return [
            'total' => $articlesArray['total'],
            'per_page' => intval($articlesArray['per_page']),
            'current_page' => $articlesArray['current_page'],
            'last_page' => $articlesArray['last_page'],
            'next_page_url' => $articlesArray['next_page_url'],
            'prev_page_url' => $articlesArray['prev_page_url'],
            'from' => $articlesArray['from'],
            'to' => $articlesArray['to'],
            'data' => $articlesArray['data']
        ];
    }

    private function transform($article)
    {
        return [
            'id' => $article['id'],
            'title' => $article['title'],
            'body' => $article['body'],
            'intro' => $article['intro'],
            'author' => $article['user']['name'],
            'comments' => $article['comments'],
            'date' => $article['created_at']->toDateTimeString()
        ];
    }
}
