<?php namespace App\Http\Middleware;

namespace App\Http\Middleware;

use Closure;

class AdminOnly
{

    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        if (!$request->user()->isAdmin()) {
            return response([
                'errors' => [
                    'You are not authorized to access this resource.'
                ]
            ], 401);
        }

        return $next($request);
    }

}