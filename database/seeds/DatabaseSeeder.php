<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Article;
use App\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->insertUsers();
        $this->insertArticles();
        Model::reguard();
    }

    private function insertUsers()
    {
        $faker = Faker\Factory::create();

        foreach (range(1, 5) as $index) {
            User::create([
                'name' => $faker->userName,
                'email' => $faker->email,
                'type' => 'regular',
                'password' => bcrypt('password')
            ]);
        }
        User::create([
            'name' => 'test',
            'email' => 'test@test.com',
            'type' => 'admin',
            'password' => bcrypt('test')
        ]);
    }

    private function insertArticles()
    {
        DB::table('articles')->delete();
        $articles = [
            [
                'user_id' => 1,
                'title' => 'Liberland, a fenntartható városállam',
                'body' => 'A miniállam 7 négyzetkilométer nagyságon terül majd el a Duna Horvátország és Szerbia közti ártéri területén, ezzel a világ harmadik legkisebb államának számít. A Liberland megépítése minden bizonnyal nagy és egyedülálló kihívást jelent majd az építészeknek, s a városka rendszerének is, hiszen az lenne a cél, hogy egy önálló én önkormányzó miniállamként üzemeljen, mely garantálja a polgárok gazdasági- és személyes szabadságát.',
                'intro' => 'Egy alga-üzemeltetésű város javaslata nyerte el a Liberland tervpályázat, mely a világ legújabb szuverén állama lesz Európában.'
            ],
            [
                'user_id' => 1,
                'title' => 'Miért bámul a borjú az új kapura?',
                'body' => 'A magyar nyelv tele van szebbnél szebb szólásokkal, melyekkel kifejezhetjük lelkiállapotunkat, érzelmeinket, vagy hangsúlyosabbá tehetjük mondandónkat. Sajnos az idő múlásával egyik-másik igazi jelentése megkopott, csak amolyan “megérzés” szerint használjuk, mert nem ismerjük pontos eredetét. Egy nemrég bekövetkezett mutatós, biciklis esést követően panaszkodtam, mondván: akkorát estem, mint az ólajtó. Aztán elgondolkodtam, hogy tulajdonképpen mekkorát is? Összeszedtem néhány régi szólást, amit nap mint nap használunk, de talán már nem tudjuk pontosan miként születtek.',
                'intro' => 'Akkorát estem, mint az ólajtó... jó, de tulajdonképpen mekkorát is? Miért hagyta ott Szent Pál az oláhokat? Mennyire ordít a fába szorult féreg? Mi az összefüggés az üveges tót és a hanyattesés között? Néhány szép, régi szólás és azok elfeledett eredete.'
            ],
            [
                'user_id' => 1,
                'title' => 'Kínában nem mindenki mongolid',
                'body' => 'Elsőre nem gondolnánk, de kínában szőke és vörös hajú ujgurok, kék szemű tádzsikok, és sötét bőrű ausztronéz népek is élnek. Az államalkotó hanok mellett Kína Taiwannal együtt értelmezett területén további 55 különböző etnikumot tartanak számon.',
                'intro' => 'Az államalkotó hanok mellett Kína Taiwannal együtt értelmezett területén további 55 különböző etnikumot tartanak számon. Elismerésükre már a Kínai Népköztársaság időszakában került sor, a korábbi gyakorlat nem különösebben foglalkozott az egyes állampolgárok etnikai besorolásával. Igaz ugyan, hogy a császárság bukása után, 1911-ben megalakult Kínai Köztársaság a zűrzavaros idők függetlenedési törekvéseit leszerelendő, az új államot öt nemzetiség, a han, tibeti, mongol, ujgur és hui hazájának jelentette be, de az ötszínű lobogó létrehozásán túl lényeges változásokkal ez sem járt, különös tekintettel arra, hogy a jellemzően periférikus területeken élő nemzetiségek egy részének ügyeibe az adott helyzetben gyakorlatban - erőforrások hiányában - nem tudtak beleszólni. Így vált kvázi függetlenné Tibet is, noha nemzetközi elismerés hiányában jogilag Kína része maradt.'
            ],
            [
                'user_id' => 1,
                'title' => 'Gyerekkori játékok',
                'body' => 'Ha ilyened volt, akkor te voltál az ovisok közt az utca királya! Bármit kérhettél a többiektől, csak azért, hogy legalább egyszer vezethessék az álomautót. Ha viszont közben más is kapott a szüleitől, azonnal megértetted, hogy mit jelent a “konkurencia” kifejezés. ÉS többé már nem te voltál a legmenőbb. Az igazi autókkal ellentétben, a pedálos kis autók értéke az idő múlásával egyáltalán nem csökkent, sőt! Manapság őrült összegekért vesztegetik a megkímélt állapotú, használt darabokat. Nem ritka, hogy 1-1 példányért akár 80.000 -100.000 Ft-ot is elkérnek. Szóval...lehetséges, hogy ma is ugyan olyan menő lennél vele, mint 30 évvel ezelőtt ;)',
                'intro' => 'Néha, baráti összejöveteleken szóba kerülnek rég elfeledettnek hitt játékok, és gyermekkori emlékek. Az ember szívesen nosztalgiázik, és ahogy végig gondolja a régi szép időket, azonnal mosolyra szalad a szája. Fiúknak és lányoknak egyaránt megvolt a kedvencük. Néhány apróságot szedtem össze, a teljesség igénye nélkül. Azokat, amik nekem sokat jelentettek akkoriban.'
            ],
            [
                'user_id' => 1,
                'title' => 'Halálos vihar és áradás Ausztráliában',
                'body' => 'Vasárnap extrém erejű felhőszakadás és viharok csaptak le Ausztrália keleti partjaira, amelynek következtében áradások alakultak ki, 26 ezer háztartás maradt áram nélkül és emberek százait kellett evakuálni.A leginkább Tasmania és Új-Dél-Wales tartományait érintő természeti események halálos áldozatokat és követeltek − a canberrai rendőrség jelentése szerint hétfőig három biztos áldozatról tudnak.',
                'intro' => 'A liberális-konzervatív kormányfő most a katasztrófák által sújtott térségeket látogatja és a klímaváltozás által okozott kockázatokra figyelmeztet.'
            ],
            [
                'user_id' => 1,
                'title' => 'Ingyen adjak a sort',
                'body' => 'Jo lenne',
                'intro' => 'Igen'
            ],
        ];
        foreach ($articles as $article) {
            Article::create($article);
        }
    }
}
