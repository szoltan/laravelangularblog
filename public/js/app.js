angular.module('app', ['app.controllers', 'satellizer', 'ui.router'])
    .service('UserService', function ($auth, $rootScope) {
        var user = JSON.parse(localStorage.getItem('user'));
        $rootScope.$on('authChanged', function (event, args) {
            user = JSON.parse(localStorage.getItem('user'));
        });
        var UserProfile = {
            isLoggedIn: function () {
                if ((user == null) && (!$auth.isAuthenticated())) {
                    $auth.logout();
                    localStorage.removeItem('user');
                    return false;
                } else {
                    return true;
                }
            },
            isAdmin: function () {
                if (this.isLoggedIn()) {
                    return (user.type == 'admin');
                } else {
                    return false;
                }
            }
        };

        return UserProfile;
    })
    .run(function ($rootScope, $auth, $state) {

        $rootScope.logout = function () {
            $auth.logout().then(function () {
                localStorage.removeItem('user');
                $rootScope.currentUser = null;
                $rootScope.$broadcast('authChanged');
                $state.go('auth');

            });
        }

        $rootScope.currentUser = JSON.parse(localStorage.getItem('user'));

    })

    .config(function ($stateProvider, $urlRouterProvider, $authProvider) {

        $authProvider.loginUrl = 'http://localhost:8000/api/authenticate';

        $stateProvider
            .state('home', {
                url: '/home',
                controller: 'AppCtrl',
                templateUrl: 'templates/home.html'
            })

            .state('articles', {
                url: '/articles',
                controller: 'ArticlesCtrl',
                templateUrl: '/templates/articles/list.html'
            })

            .state('articleEdit', {
                url: '/articles/edit/{articleId:int}',
                controller: 'ArticleEditCtrl',
                templateUrl: '/templates/articles/edit.html'
            })

            .state('articleShow', {
                url: '/articles/show/{articleId:int}',
                controller: 'ArticleShowCtrl',
                templateUrl: '/templates/articles/show.html'
            })

            .state('articleAdd', {
                url: '/articles/add',
                controller: 'ArticleAddCtrl',
                templateUrl: '/templates/articles/add.html'
            })

            .state('auth', {
                url: '/auth',
                templateUrl: 'templates/login.html',
                controller: 'AuthCtrl'
            })
        $urlRouterProvider.otherwise('/home');
    })
    .directive('serverError', function () {
        return {
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ctrl) {
                return element.on('change keyup', function () {
                    return scope.$apply(function () {
                        return ctrl.$setValidity('server', true);
                    });
                });
            }
        };
    });