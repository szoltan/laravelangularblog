angular.module('app.controllers', ['toastr'])
    .config(function (toastrConfig) {
        angular.extend(toastrConfig, {
            allowHtml: false,
            closeButton: true,
            closeHtml: '<button>&times;</button>',
            extendedTimeOut: 1000,
            iconClasses: {
                error: 'toast-error',
                info: 'toast-info',
                success: 'toast-success',
                warning: 'toast-warning'
            },
            messageClass: 'toast-message',
            onHidden: null,
            onShown: null,
            onTap: null,
            progressBar: true,
            tapToDismiss: true,
            timeOut: 2000,
            titleClass: 'toast-title',
            toastClass: 'toast'
        });
    })
    .controller('AppCtrl', function ($scope, $timeout, $rootScope, $auth) {

        $scope.isLoggedIn = $auth.isAuthenticated();
        $scope.$on('authChanged', function (event, args) {
            $scope.isLoggedIn = $auth.isAuthenticated();
        });
    })

    .controller('AuthCtrl', function ($scope, $location, $http, $state, $auth, $rootScope, toastr) {

        $scope.loginData = {}
        $scope.loginError = false;
        $scope.loginErrorText;

        $scope.login = function () {

            var credentials = {
                email: $scope.loginData.email,
                password: $scope.loginData.password
            }

            $auth.login(credentials).then(function () {
                $http.get('http://localhost:8000/api/authenticate/user').success(function (response) {
                        var user = JSON.stringify(response.user);

                        // Set the stringified user data into local storage
                        localStorage.setItem('user', user);

                        // Getting current user data from local storage
                        $rootScope.currentUser = response.user;
                        // $rootScope.currentUser = localStorage.setItem('user');;
                        $rootScope.$broadcast('authChanged');
                        $state.go('home');
                    })
                    .error(function () {
                        toastr.error('asd', 'Error');

                    })
            });
        }

    })
    .controller('ArticleEditCtrl', function ($scope, $stateParams, $auth, $rootScope, $http, $stateParams, UserService, toastr) {
        $http({
            url: 'http://localhost:8000/api/v1/articles/' + $stateParams.articleId,
            method: "GET",
        }).success(function (article, status, headers, config) {
            $scope.article = article.data;

        });

        $scope.save = function () {
            $http.put('http://localhost:8000/api/v1/articles/' + $scope.article.id, {
                body: $scope.article.body,
                title: $scope.article.title,
                intro: $scope.article.intro
            }).success(function (response) {
                toastr.success('Article updated', 'Success');
            }).error(function (response) {
                angular.forEach(response.errors, function (error, field) {
                    if (field in $scope.form) {
                        $scope.form[field].$setValidity('server', false);
                        $scope.form[field].errorMessage = error;
                    } else {
                        toastr.error(error, 'Error');
                    }
                });

            });
        }
    })
    .controller('ArticleShowCtrl', function ($scope, $stateParams, $auth, $rootScope, $http, $stateParams, toastr) {
        $scope.isLoggedIn = $auth.isAuthenticated();
        $scope.comment = '';
        $scope.init = function () {
            $http({
                url: 'http://localhost:8000/api/v1/articles/' + $stateParams.articleId,
                method: "GET",
            }).success(function (response, status, headers, config) {
                $scope.article = response.data;
            });
        }

        $scope.addComment = function () {
            $http.post('http://localhost:8000/api/v1/articles/' + $scope.article.id + '/comments', {
                content: $scope.comment,
            }).success(function (response) {
                toastr.success('Comment added', 'Success');
                $scope.init();
                $scope.comment = '';
            }).error(function (response) {
                angular.forEach(response.errors, function (error, field) {
                    if (field in $scope.form) {
                        $scope.form[field].$setValidity('server', false);
                        $scope.form[field].errorMessage = error;
                    } else {
                        toastr.error(error, 'Error');
                    }
                });
            });
        }

        $scope.init();
    })
    .controller('ArticleAddCtrl', function ($scope, $stateParams, $auth, $rootScope, $http, toastr) {
        $scope.article = {title: "", body: "", intro: ""}
        $scope.save = function () {
            $http.post('http://localhost:8000/api/v1/articles', {
                title: $scope.article.title,
                body: $scope.article.body,
                intro: $scope.article.intro,
            }).success(function (response) {
                toastr.success('Article saved', 'Success');
            }).error(function () {
                toastr.error(error, 'Error');
            });
        }
    })
    .controller('ArticlesCtrl', function ($scope, $stateParams, $auth, $rootScope, $http, UserService, toastr) {
        $scope.articles = [];
        $scope.error;
        $scope.isAdmin = UserService.isAdmin();
        $scope.listCanSwipe = true;
        $scope.searchTerm = '';
        $scope.lastpage = 1;

        $scope.init = function () {
            $scope.lastpage = 1;
            $http({
                url: 'http://localhost:8000/api/v1/articles',
                method: "GET",
                params: {page: $scope.lastpage, search: $scope.searchTerm}
            }).success(function (articles, status, headers, config) {
                $scope.articles = articles.data;
                $scope.currentpage = articles.current_page;
                $scope.noMoreItemsAvailable = (articles.data.length == 0);
            });
        };
        $scope.noMoreItemsAvailable = false;

        $scope.loadMore = function (limit) {
            if (!limit) {
                limit = 5;
            }

            $scope.lastpage += 1;
            $http({
                url: 'http://localhost:8000/api/v1/articles',
                method: "GET",
                params: {limit: limit, page: $scope.lastpage}
            }).success(function (articles, status, headers, config) {
                if (articles.next_page_url == null) {
                    $scope.noMoreItemsAvailable = true;
                }
                $scope.articles = $scope.articles.concat(articles.data);
            });
        };

        $scope.deleteArticle = function (index, articleId) {
            $http.delete('http://localhost:8000/api/v1/articles/' + articleId)
                .success(function () {
                    $scope.articles.splice(index, 1);
                    toastr.success('Article deleted', 'Success');
                });
            ;
        }

        $scope.init();
    });