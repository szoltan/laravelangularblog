<!DOCTYPE html>
<html ng-app="app">

<head>
    <link rel="stylesheet" href="components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="components/components-font-awesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="components/angular-toastr/dist/angular-toastr.css"/>
    <title>OMG!!! This is a blog.</title>

    <script src="components/angular/angular.min.js"></script>
    <script src="components/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script src="components/satellizer/satellizer.min.js"></script>
    <script src="components/angular-resource/angular-resource.min.js"></script>
    <script src="components/ngstorage/ngStorage.min.js"></script>
    <script src="components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
    <script src="components/angular-toastr/dist/angular-toastr.tpls.min.js"></script>

    <script src="js/app.js"></script>
    <script src="js/controllers.js"></script>
</head>
<body>

<nav class="navbar navbar-default" ng-controller="AppCtrl">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">Blog</a>
        </div>

        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="#">
                    <i class="fa fa-home" aria-hidden="true"></i> Home
                </a>
            </li>
            <li>
                <a href="#/articles">
                    <i class="fa fa-newspaper-o" aria-hidden="true"></i> Articles
                </a>
            </li>
            <li ng-hide="isLoggedIn">
                <a href="#/auth">
                    <i class="fa fa-sign-in"></i> Sign In
                </a>
            </li>
            <li ng-hide="!isLoggedIn">
                <a ng-click="logout()" href="#">
                    <i class="fa fa-sign-out"></i> Sign out
                </a>
            </li>
        </ul>
    </div>
</nav>

<div class="container">

    <div ui-view></div>

</div>
</body>

</html>